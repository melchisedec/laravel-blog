<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    @include( 'partials.head' )    
  </head>
  
  <body>
    @include( 'partials.nav' )
    
    <div class="container">
    	@yield( 'content' )        
    </div>
    @include( 'partials.messages' )

    @include( 'partials.footer' )
    @yield( 'scripts' )    
  </body>
</html>