@extends( 'main' )
@section( 'title', '| View Post' )

@section( 'content' )
        <div class="row">
            <div class="col-md-7">
                  <h1>{{ $post->title }} </h1>
                  <p>{{ $post->body }} </p>
                  <hr>

                  <div class="tags">
                    @foreach( $post->tags as $tag )
                      <span class="label label-default">{{ $tag->name }}</span>
                    @endforeach
                  </div>
            </div>
            <div class="col-md-5">
                <div class="well">
                    <dl class="dl-horizontal">
                      <dt>Url</dt>
                      <dd>
                        <a href="{{ url( $post->slug ) }}">{{ url( $post->slug ) }}</a>
                      </dd>
                    </dl>
                    <dl class="dl-horizontal">
                      <dt>Category</dt>
                      <dd>{{ $post->category->name }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                      <dt>created at</dt>
                      <dd>{{ date( 'M j, Y h:i a', strtotime( $post->created_at ) ) }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                      <dt>Last updated at</dt>
                      <dd>{{ date( 'M j, Y h:i a', strtotime( $post->updated_at ) ) }}</dd>
                    </dl>
                    <hr>
                    <div class="row" >
                        <div class="col-sm-6">
                            {!! Html::linkRoute( 'posts.edit', 'Edit', array( $post->id ), array( 'class' => 'btn btn-primary btn-block')  ) !!}
                                     
                            
                        </div>
                        <div class="col-sm-6">
                          {!! Form::open( [ 'route' => [ 'posts.destroy', $post->id ], 'method' => 'DELETE' ] ) !!}

                          {!! Form::submit( 'Delete', [ 'class' => 'btn btn-danger btn-block' ] ) !!}
                          {!! Form::close() !!}

                        </div>
                    </div>
                </div>                
            </div>
        </div>
@endsection

