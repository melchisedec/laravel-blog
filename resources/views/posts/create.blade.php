@extends( 'main' )
@section( 'title', '| Create new post' )

@section( 'stylesheet' )
{!! Html::style( 'css/select2.min.css' )  !!}

@section( 'content' )

        <div class="row">
            <div class="col-md-8">
                  <h1>Create New post!</h1>
                  <hr>
                    {!! Form::open(['route' => 'posts.store']) !!}
                        {{ Form::label( 'title', 'Title' ) }}
                        {{ Form::text( 'title', null, array( 'class' => 'form-control' )) }}

                        {{ Form::label( 'slug', 'Slug:' ) }}
                        {{ Form::text( 'slug', null, array( 'class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255' ) ) }}

                        {{ Form::label( 'category_id', 'Category' ) }}
                        <select class="form-control" name="category_id">
                            @foreach( $categories as $category )
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach                            
                        </select>

                        {{ Form::label( 'tags', 'Tag' ) }}
                        <select class="form-control multiple-select" name="tags[]" multiple="multiple">
                            @foreach( $tags as $tag )
                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                            @endforeach                            
                        </select>




                        {{ Form::label( 'body', 'Body' ) }}
                        {{ Form::textarea( 'body', null, array( 'class' => 'form-control' )) }}

                        {{ Form::submit( 'Create Post', array( 'class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:10px;' )) }}
                    {!! Form::close() !!}
            </div>
        </div>

@endsection

@section('scripts')
{!! Html::script( 'js/select2.min.js' ) !!}

<script type="text/javascript">
    $('.multiple-select').select2();
</script>
@endsection