@extends( 'main' )
@section( 'title', '| Edit Blog Post' )

@section( 'stylesheet')

{!! Html::style( 'css/style.min.css' )  !!}
{!! Html::style( 'css/select2.min.css' )  !!}

@endsection

@section( 'content' )
        <div class="row">
        	{!! Form::model( $post, [ 'route' => [ 'posts.update', $post->id ], 'method' => 'PUT' ] ) !!}
            <div class="col-md-7">
            	  {{ Form::label( 'title', 'Title:' ) }}
                  {{ Form::text( 'title', null, [ 'class' => 'form-control input-lg' ] ) }}

                  {{ Form::label( 'slug', 'Slug:' ) }}
                  {{ Form::text( 'slug', null, [ 'class' => 'form-control' ] ) }}

                  {{ Form::label( 'category_id', 'Category' ) }}
                        {{ Form::select( 'category_id', $categories, null, [ 'class' => 'form-control' ] ) }}

                  {{ Form::label( 'tags', 'Tags:' ) }}
                  {{ Form::select( 'tags[]', $tags, null, [ 'class' => 'multiple-select form-control', 'multiple' => 'multiple' ] )}}

                  {{ Form::label( 'body', 'Body:',  [ 'class' => 'form-spacing-top' ] ) }}
                  {{ Form::textarea( 'body', null, [ 'class' => 'form-control' ] ) }}
            </div>
            <div class="col-md-5">
                <div class="well">
                    <dl class="dl-horizontal">
                      <dt>created at</dt>
                      <dd>{{ date( 'M j, Y h:i a', strtotime( $post->created_at ) ) }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                      <dt>Last updated at</dt>
                      <dd>{{ date( 'M j, Y h:i a', strtotime( $post->updated_at ) ) }}</dd>
                    </dl>
                    <hr>
                    <div class="row" >
                        <div class="col-sm-6">
                            {!! Html::linkRoute( 'posts.show', 'Cancel', array( $post->id ), array( 'class' => 'btn btn-danger btn-block')  ) !!}
                                     
                            
                        </div>
                        <div class="col-sm-6">
                        	{{ Form::submit( 'Save changes', [ 'class' => 'btn btn-success btn-block' ] ) }}
                            

                        </div>
                    </div>
                </div>                
            </div>
            {!! Form::close() !!}
        </div>
@endsection

@section('scripts')
{{-- {!! json_encode( $post->tags()->getRelatedIds() ) !!} --}}
{!! Html::script( 'js/select2.min.js' ) !!}
<script type="text/javascript">
    $('.multiple-select').select2();
    // $('.multiple-select').select2().val(
      
    //   ).trigger( 'change' );
</script>

@endsection

