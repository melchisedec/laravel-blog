@extends( 'main' )
@section( 'title', '| About' )

@section( 'stylesheet' )
    <link rel="stylesheet" type="text/css" href="styles.css">
@endsection


@section( 'content' )
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                  <h1>About to my Blog!</h1>
                  <p class="lead">Thank you so much for visiting. This is my test site with laravel </p>
                </div>
            </div>
        </div>
@endsection

@section( 'scripts' )
    <script type="text/javascript" src="scrips.js"></script>
@endsection