<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Laravel blog - @yield( 'title' ) </title>
<link href="/css/bootstrapx.min.css" rel="stylesheet">
{{ Html::style( 'css/bootstrap.min.css' ) }}

@yield( 'stylesheet' )