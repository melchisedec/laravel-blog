<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group( [ 'middleware' => [ 'web' ] ], function(){

	Route::get('blog/{slug}', ['as' => 'blog.single', 'uses' => 'BlogController@getSingle'])->where('slug', '[\w\d\-\_]+');

	//Categories
	Route::resource( 'categories', 'CategoryController', [ 'except' => [ 'create' ] ] );

	//Tags
	Route::resource( 'tags', 'TagController', [ 'except' => [ 'create' ] ] );




	Route::get( 'blog/{slug}', [ 'as' => 'blog.single', 'uses' => 'BlogController@getSingle' ] )->where( 'slug', '[\w\d\-\_]+' );
	Route::get( 'blog', [ 'uses' => 'BlogController@getIndex', 'as' => 'blog.index' ] );
	Route::get( '/contact', 'PagesController@getContact' );
	Route::post( '/contact', 'PagesController@postContact' );

// Route::get('contact', 'ContactController@create')->name('contact.create');
// Route::post('contact', 'ContactController@store')->name('contact.store');

	Route::get( '/about', 'PagesController@about' );
	Route::get( '/', 'PagesController@index' );
	Route::resource( 'posts', 'PostController' );
});

Route::get( 'mail', 'mailController@index');
Route::post( 'postMail', 'mailController@post' );

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Auth::logout();